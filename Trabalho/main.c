#include <stdlib.h>
#include <stdio.h>
#include "tree.h"
#include "file_reader.h"

/*
	NOTAS GERAIS: 
	- TODOS OS METODOS RECEBEM COMO PARAMETRO UM VALOR PARA PRINT:
		- CASO PRINT = 1, OS PRINTS DO METODO SAO REALIZADOS
		- CASO PRINT = 0, OS PRINTS SÃO IGNORADOS
		OS PRINTS SAO ATIVADOS PELO USUARIO PELO MENU DE OPCOES (OPCAO 6)

	
*/


int main(int argc, char * argv[]) {
	system("clear");
	char * filename;
	if(argc == 2){
		filename = argv[1];		
	}else if(argc < 2){
    	printf("\033[1;31mPROGRAMA INICIADO SEM INSTANCIA DE ENTRADA. ENTRE COM UMA INSTANCIA DA SEGUINTE FORMA: './program <caminho para a instancia>'\033[0m\n");
  	}
	Tree * t = (Tree*)malloc(sizeof(Tree));
	createTree(0,t);
	// MENU DE OPCOES EM LOOP. 
  	int answer = 0;
  	int print_switch = 0;
  	int value;
  	while(answer != -1){
  		if(answer == 0)
        printf("ESCOLHA UMA DAS ACOES ABAIXO:\n1)Adicionar No\n2)Remover No\n3)Consultar No\n4)Ler Instancia Especificada\n5)Mostrar arvore\n6)Ativar/desativar informacoes auxiliares(%d)\n7)Limpar a tela\n8)Salvar arvore em arquivo (precisa especificar a extensao)\n!(1,2,3,4,5,6,7,8))Sair\n",print_switch);
      else
        printf("\n\n\n\n\n\n\n\n\n\nESCOLHA UMA DAS ACOES ABAIXO:\n1)Adicionar No\n2)Remover No\n3)Consultar No\n4)Ler Instancia Especificada\n5)Mostrar arvore\n6)Ativar/desativar informacoes auxiliares(%d)\n7)Limpar a tela\n8)Salvar arvore em arquivo (precisa especificar a extensao)\n!(1,2,3,4,5,6,7,8))Sair\n",print_switch);
  		scanf("%d", &answer);
  		switch(answer){
  			case(1):
  				printf("ESCREVA UMA CHAVE PARA O NUMERO:\n");
  				scanf(" %d", &value);
  				Node * newNode = (Node*)malloc(sizeof(Node));
  				createNode(print_switch,newNode, value);
  				addNode(print_switch,t, newNode);
  				break;
  			case(2):
  				printf("ESCREVA UMA CHAVE PARA REMOVER:\n");
  				scanf(" %d", &value);
          Node * toRemove = (Node*)malloc(sizeof(Node));
          createNode(0,toRemove, value);
          int r = removeNode(print_switch,t,toRemove);
          free(toRemove);
  				break;
  			case(3):
  				printf("PROCURAR POR NO:\n");
  				scanf(" %d", &value);
  				Node * searchNode = (Node*)malloc(sizeof(Node));
  				createNode(0,searchNode, value);
  				checkNodeExistance(print_switch, searchNode, t);
  				free(searchNode);
  				break;
  			case(4):
                if (filename == NULL){
                    printf("\nINSTÂNCIA NÃO ESPECIFICADA NA INICIALIZAÇÃO.\nEXECUTE NOVAMENTE CARREGANDO O ARQUIVO E SELECIONE ESTA OPÇÃO.\n\n");
                } else {
  				readInstance(print_switch,filename, t);
                }
  				break;
  			case(6):
  				if(print_switch == 0)
  					print_switch = 1;
  				else
  					print_switch = 0;
  				break;
  			case(5):
  				print(t);
  				getchar();
  				break;
  			case(7):
  				system("clear");
  				break;
        case(8):
          printf("DIGITE O NOME DO ARQUIVO(MAX 20 CHARS):\n");
          char savename[20];
          scanf(" %s",&savename);
          saveTree(t, savename);
          break;	
  			default:
  				answer = -1;
  		}
  		
  	}
	return 0;
}
