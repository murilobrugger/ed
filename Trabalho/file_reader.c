
#include "file_reader.h"


void readInstance(int print_switch, char* instance, Tree * t){
	
	FILE* file = fopen (instance, "r");
  	int value = 0;

  	fscanf (file, "%d", &value);    
  	while (!feof (file)) {
  		Node * newNode = (Node*)malloc(sizeof(Node));
  		createNode(print_switch,newNode, value);
  		addNode(print_switch,t, newNode);
  		if(print_switch == 1){
	  		print(t);
	  		printf("**********************************\n");
	  		getchar();
  		}
      	fscanf (file, "%d", &value);      
    }
  	fclose (file);
}