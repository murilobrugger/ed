#include <stdio.h>
#include "print_tree.h"
#define COUNT 5

void print2DTree(Node *root, int space) 
{ 
    if (root == NULL) 
        return; 
    space += COUNT; 
  
    print2DTree(root->right_son, space); 
  
    printf("\n"); 
    for (int i = COUNT; i < space; i++) 
        printf(" "); 
    printf("%d(%d,%d)\n", root->value, root->left_depth, root->right_depth); 
  
    print2DTree(root->left_son, space); 
} 

void print(Tree* t){
	print2DTree(t->root, 0);
}
