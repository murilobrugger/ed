#ifndef _FILE_READER_H
#define _FILE_READER_H
#include "tree.h"
#include <stdio.h>
#include <stdlib.h>

void readInstance(int print_switch, char* file, Tree* t);


#endif