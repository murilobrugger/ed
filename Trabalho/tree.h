#ifndef TREE_H
#define TREE_H
#include <stdlib.h>
#include <stdio.h>

struct node{
	struct node * left_son;
	struct node * right_son;
	int value;
	int balance;
	int right_depth;
	int left_depth;
} typedef Node;

struct tree {
	Node * root;
	int biggest_depth;
	int num_nodes;
} typedef Tree; 

void print(Tree* t); 
void createTree(int print, Tree * t);
int addNode(int print, Tree * t, Node * n);
int removeNode(int print, Tree * t, Node * n);
void saveTree(Tree * t, char * file);
void createNode(int print, Node * n, int value);
int findNodePosition(int mode, int print, Tree* t, Node *n, Node * new, int * depth, Node * father);
void checkNodeExistance(int print_switch, Node * n, Tree * t);

#endif