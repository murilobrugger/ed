#include "tree.h"
#define COUNT 10

void createTree(int print, Tree * t){ // INICIALIZA ARVORE
	t->root = NULL;
	t->biggest_depth = 0;
	t->num_nodes = 0;
	if(print == 1)
		printf("\n\n\n\n\n\nARVORE CRIADA COM SUCESSO!\n");
}


void navegateNode(int level, Node *n, int * heap){
	printf("heap level: %d, value: %d\n", level, n->value);
	if(level == 0){
		heap[level] = n->value;
		if(n->left_son != NULL)
			navegateNode(level + 1, n->left_son, heap);
		if(n->right_son != NULL)
			navegateNode(level + 2, n->right_son, heap);
	} else{
		heap[level] = n->value;
		if(n->left_son != NULL)
			navegateNode(level*2 + 1, n->left_son, heap);
		if(n->right_son != NULL)
			navegateNode(level*2 + 2, n->right_son, heap);
	}

}

void saveTree(Tree * t, char * file){
	FILE* fileManager = fopen (file, "w");
	int *heap = (int*)malloc(sizeof(int) * t->num_nodes*2 + 1);
	printf("heapsize: %d, num_nodes: %d\n", t->num_nodes*2 + 1, t->num_nodes);
	for(int i = 0; i < t->num_nodes*2 + 1; i++)
		heap[i] = -10;
 	navegateNode(0, t->root, heap);
 	for(int i = 0; i < t->num_nodes*2 + 1; i++){
		if(heap[i] != -10){
			printf("%d,",heap[i]);
			fprintf(fileManager, "%d\n", heap[i]);
		}
 	}
  	fclose (fileManager);	
  	printf("\n\n\n\n\n\nARVORE GRAVADA COM SUCESSO!\n");
}

void checkNodeExistance(int print_switch, Node * n, Tree* t){ // FACHADA PARA BUSCA DO NO. EH CHAMADO findNodePosition USANDO MODE = 0
	int depth = 0;
	if(t->root != NULL){
		int value = findNodePosition(0, print_switch, t, t->root, n, &depth, NULL);
		if(print_switch == 1){
			printf("COMPARACOES: %d\n", depth);	
		}
		if(value < 0){
			printf("\n\n\n\n\n\nNO ENCONTRADO!\n\n\n");
		} else
			printf("\n\n\n\n\n\nNO NAO ENCONTRADO!\n\n\n");
	} else
		printf("\n\n\n\n\n\nNO NAO ENCONTRADO!\n\n\n");
}

int addNode(int print, Tree * t, Node * n){ // ADICIONA NOVO NO NA ARVORE. BUSCA DA POSICAO EH FEITA PELO findNodePosition USANDO MODE = 1
	if(t->root == NULL){
		t->root = n;
		t->num_nodes++;
		return 1;
	}
	int depth = 0;														// A VARIAVEL DEPTH GUARDA A PROFUNDIDADE PERCORRIDA, LOGO O NUM COMPARACOES
	int new_depth = findNodePosition(1,print,t,t->root,n,&depth, NULL);  
	if(print == 1)
		printf("COMPARACOES: %d\n", depth);
	if (new_depth < 0)
		return 0;
	t->num_nodes++;
	if (new_depth > t->biggest_depth){
		t->biggest_depth = new_depth;
	}
	return new_depth;
}

int removeNode(int print, Tree * t, Node * n){
    if (t->root == NULL){
        if (print == 1){
            printf("ÁRVORE VAZIA.\n");
        }
        return -1;
    }
    t->num_nodes--;
    int depth = 0;
    int new_depth = findNodePosition(2,print,t,t->root,n,&depth, NULL);
    if(print == 1)
        printf("COMPARACOES: %d\n", depth);
    if (new_depth < 0)
        return 0;
    if (new_depth > t->biggest_depth)
        t->biggest_depth = new_depth;
    return new_depth;
}

void createNode(int print, Node * n, int value){ // INICIALIZA NO COM VALORES VAZIOS
	n->left_son = NULL;
	n->right_son = NULL;
	n->value = value;
	n->balance = 0;
	n->right_depth = 0;
	n->left_depth = 0;
	if(print == 1)
		printf("\n\n\n\n\n\nNO %d CRIADO COM SUCESSO!\n",value);
}

   //     c
   //      \
   //       b
   //      / \
   //     a   d
   
   //   b
   //  / \
   // c   d
   //  \  
   //   a


void LL(Tree * t, Node *n, Node* father){  // USADO SE ARVORE ESTA DESBALANCEADA NO LADO DIREITO.
	Node* son = n->right_son;
	n->right_son = son->left_son;
	son->left_son = n;
	if(father != NULL){
		if(father->left_son != NULL){
			if(father->left_son->value == n->value)
				father->left_son = son;
			else
				father->right_son = son;	
		} else 
			father->right_son = son;
	} else
		t->root = son;
	n->right_depth = son->left_depth;
	n->balance = n->left_depth - n->right_depth;
	int new_n_left_depth = n->left_depth;
	if(n->right_depth > new_n_left_depth)
		new_n_left_depth = n->right_depth;
	son->left_depth = new_n_left_depth + 1;
	son->balance = son->left_depth - son->right_depth;
}

   //     c(2,0)
   //    /
   //   b(1,1)
   //  / \
   // a   d
   
   //   b (, max(c) + 1)
   //  / \
   // a   c ( max(d) + 1, 0)
   //    /
   //   d

void RR(Tree * t, Node *n, Node* father){  // USADO SE ARVORE ESTA DESBALANCADA NO LADO ESQUERDO.
	Node* son = n->left_son;
	n->left_son = son->right_son;
	son->right_son = n;
	if(father != NULL) {
		if(father->left_son != NULL){
			if(father->left_son->value == n->value)
				father->left_son = son;
			else
				father->right_son = son;
		} else 
			father->right_son = son;
	} else
		t->root = son;
	n->left_depth = son->right_depth;
	n->balance = n->left_depth - n->right_depth;
	int new_n_left_depth = n->left_depth;
	if(n->right_depth > new_n_left_depth)
		new_n_left_depth = n->right_depth;
	son->right_depth = new_n_left_depth + 1;
	son->balance = son->left_depth - son->right_depth;
	
	
}


void printTree(Node *root, int space) // METODO PARA PRINTAR ARVORE
{ 
    if (root == NULL) 
        return; 
    space += COUNT; 
  
    printTree(root->right_son, space); 
  
    printf("\n"); 
    for (int i = COUNT; i < space; i++) 
        printf(" "); 
    printf("%d(%d,%d)\n", root->value, root->left_depth, root->right_depth); 
  
    printTree(root->left_son, space); 
} 

void print(Tree* t){
	printTree(t->root, 0);
}

Node* treeRotation(int print, Tree * t, Node *n, Node * father){ // FACHADA PARA IDENTIFICACAO DE TIPO DE ROTACAO.
	if(n->balance > 0){ 		// tree unbalanced on left side | R rotation 
		Node* left_node = n->left_son;
		if(left_node->right_depth > left_node->left_depth){ // a rotation is needed on son | RL rotation
			if(print == 1)
				printf("ROTACAO RL NECESSARIA NO NO: %d\n", n->value);
			LL(t, left_node, n);
		} else{
			if(print == 1)
				printf("ROTACAO RR NECESSARIA NO NO: %d\n", n->value);
		}
		if(print == 1){
			printf("**********************************\n");
			getchar();
		}
		RR(t, n, father);
		return left_node;
	} else{ 					// tree unbalanced on the right side | L rotation
		Node* right_node = n->right_son;
		if(right_node->left_depth > right_node->right_depth){ // a rotation is needed on son | LR rotation
			if(print == 1)
				printf("ROTACAO LR NECESSARIA NO NO: %d\n", n->value);
			RR(t, right_node, n);
		} else{
			if(print == 1)
				printf("ROTACAO LL NECESSARIA NO NO: %d\n", n->value);
		}
		if(print == 1){
			printf("**********************************\n");
			getchar();
		} 
		LL(t, n, father);
		return right_node;
	}	
}

int lookThroughLeft(int print, Node * n, Node * father, Tree * t){
	if(print == 1)
		printf("n: %d, father: %d\n",n->value, father->value);
	int value;
	if(n->right_son != NULL){
		value = lookThroughLeft(print, n->right_son, n, t);
		if(n->right_son != NULL){
			int new_depth = n->right_son->left_depth;
			if(n->right_son->right_depth > new_depth)
				new_depth = n->right_son->right_depth;
			n->right_depth = new_depth + 1;
		}
	}else{
		if (n->left_son != NULL){
			if(father->left_son != NULL){
				if(father->left_son->value == n->value){
					father->left_son = n->left_son;
					int new_depth = n->left_son->left_depth;
					if(n->left_son->right_depth > new_depth)
						new_depth = n->left_son->right_depth;
					father->left_depth = new_depth + 1;
				} else{
					father->right_son = n->left_son;
					int new_depth = n->left_son->left_depth;
					if(n->left_son->right_depth > new_depth)
						new_depth = n->left_son->right_depth;
					father->right_depth = new_depth + 1;
				}
			}else{
				father->right_son = n->left_son;
				int new_depth = n->left_son->left_depth;
				if(n->left_son->right_depth > new_depth)
					new_depth = n->right_son->right_depth;
				father->right_depth = new_depth + 1;
			}
		} else{
			if(father->left_son != NULL){
				if(father->left_son->value == n->value){
					father->left_depth--;
					father->left_son = NULL;
				} else{
					father->right_depth--;
					father->right_son = NULL;
				}
			}else{
				father->right_depth--;
				father->right_son = NULL;
			}
		}
		value = n->value;
		free(n);
		return value;
	}
	n->balance = n->left_depth - n->right_depth;
	if(abs(n->balance) > 2){
		if(print == 1)
			printTree(t->root,0);
		n = treeRotation(1,t, n, father);	
	}
	return value;

}

int lookThroughRight(int print, Node * n, Node * father, Tree * t){
	if(print == 1)
		printf("n: %d, father: %d\n",n->value, father->value);
	int value;
	if(n->left_son != NULL){
		value = lookThroughRight(print, n->left_son, n, t);
		if(n->left_son != NULL){
			int new_depth = n->left_son->left_depth;
			if(n->left_son->right_depth > new_depth)
				new_depth = n->left_son->right_depth;
			n->left_depth = new_depth + 1;
		}
	}else{
		if (n->right_son != NULL){
			if(father->left_son != NULL){
				if(father->left_son->value == n->value){
					father->left_son = n->right_son;
					int new_depth = n->right_son->left_depth;
					if(n->right_son->right_depth > new_depth)
						new_depth = n->right_son->right_depth;
					father->left_depth = new_depth + 1;
				} else{
					father->right_son = n->right_son;
					int new_depth = n->right_son->left_depth;
					if(n->right_son->right_depth > new_depth)
						new_depth = n->right_son->right_depth;
					father->right_depth = new_depth + 1;
				}
			}else{
				father->right_son = n->right_son;
				int new_depth = n->right_son->left_depth;
				if(n->right_son->right_depth > new_depth)
					new_depth = n->right_son->right_depth;
				father->right_depth = new_depth + 1;
			}
		} else{
			if(father->left_son != NULL){
				if(father->left_son->value == n->value){
					father->left_depth--;
					father->left_son = NULL;
				} else{
					father->right_depth--;
					father->right_son = NULL;
				}
			}else{
				father->right_depth--;
				father->right_son = NULL;
			}
		}
		value = n->value;
		free(n);
		return value;
	}
	n->balance = n->left_depth - n->right_depth;
	if(abs(n->balance) > 2){
		if(print == 1)
			printTree(t->root,0);
		n = treeRotation(1,t, n, father);	
	}
	return value;
}

int findNodePosition(int mode, int print, Tree * t, Node *n, Node * new, int * depth, Node * father){  // METODO MULTI-FUNCAO PARA BUSCA DE NO NA ARVORE.
	int new_depth = 0;                                                                                 // COM MODE = 0 PROCURA A EXISTENCIA DO NÓ.
	*depth = *depth + 1;                                                                               // COM MODE = 1 PROCURA A POSICAO DE INSERCAO E INSERE.
	if(mode == 0){                                                                                     // COM MODE = 2 PROCURA A POSICAO DE REMOCAO E REMOVE.
		if(n->value == new->value)
			return -1;
		else{
			int is_left = 0, is_right = 0;
			if(new->value < n->value){
				if(n->left_son != NULL)
					is_left = findNodePosition(mode, print, t, n->left_son, new, depth, n);
				else
					is_left = -1;
			}else{
				if(n->right_son != NULL)
					is_right = findNodePosition(mode, print, t, n->right_son, new, depth, n);
				else
					is_right = -1;
			}	
			if(is_right < 0 || is_left < 0)
				return -1;
			return 0;
		}
	} else if(mode == 1){
		if(n->value == new->value)
			return -1;
		else if(n->value > new->value){
			if(n->left_son == NULL){
				n->left_son = new;
				n->left_depth++;
				n->balance = n->left_depth - n->right_depth;
				if(abs(n->balance) >= 2){
					if(print == 1)
						printTree(t->root,0);
					n = treeRotation(print,t, n, father);
				}
				if(n->left_depth > n->right_depth)
					return n->left_depth;
				return n->right_depth;
			}
			new_depth = findNodePosition(mode, print, t, n->left_son, new, depth, n); 
			n->left_depth = new_depth + 1;			
		} else{
			if(n->right_son == NULL){
				n->right_son = new;
				n->right_depth++;
				n->balance = n->left_depth - n->right_depth;
				if(abs(n->balance) >= 2){
					if(print == 1)
						printTree(t->root,0);
					n = treeRotation(print,t, n, father);
				}
				if(n->left_depth > n->right_depth)
					return n->left_depth;
				return n->right_depth;
			}
			new_depth = findNodePosition(mode,print,t, n->right_son, new, depth, n);
			n->right_depth = new_depth + 1;
		}
		n->balance = n->left_depth - n->right_depth;
		if(abs(n->balance) >= 2){
			if(print == 1)
				printTree(t->root,0);
			n = treeRotation(print,t, n, father);
		}
		if(n->left_depth > n->right_depth)
			return n->left_depth;
		return n->right_depth;
    } else if (mode == 2){
        if (n->value == new->value){
            if ((n->left_son == NULL) && (n->right_son == NULL)){
                if (print == 1){
                    printf("O no a ser removido e uma folha.\n");
                }
                if(father != NULL){
	                if (father->left_son == n){
	                    father->left_son = NULL;
	                } else {
	                    father->right_son = NULL;
	                }
            	} else{
            		t->root = NULL;
            	}
                if (print == 1){
                    printf("No removido com sucesso.\n");
                }
                free(n);
                
                return 0;
            } else {
                if (print == 1){
                    printf("O no a ser removido nao e uma folha.\n");
                }
                Node* pathNode = (Node*)malloc(sizeof(Node));
                Node* pathFather = (Node*)malloc(sizeof(Node));
                int value;
                if (n->right_son != NULL){
                    if (print == 1){
                        printf("Buscar o melhor no na subarvore direita para substituir o no a ser excluido.\n");
                        printf("lookThroughRight\n");
                    }
                    value = lookThroughRight(print, n->right_son, n, t);
                    n->value = value;
                    if(n->right_son != NULL){
						int new_depth = n->right_son->left_depth;
						if(n->right_son->right_depth > new_depth)
							new_depth = n->right_son->right_depth;
						n->right_depth = new_depth+1;
					}
                } else {
                	if (print == 1){
                        printf("Buscar o melhor no na subarvore esquerda para substituir o no a ser excluido.\n");
                    	printf("lookThroughLeft\n");
                    }
                    value = lookThroughLeft(print, n->left_son, n, t);
                    n->value = value;
                    if(n->left_son != NULL){
						int new_depth = n->left_son->left_depth;
						if(n->left_son->right_depth > new_depth)
							new_depth = n->left_son->right_depth;
						n->left_depth = new_depth+1;
					}
                }
                n->balance = n->left_depth - n->right_depth;
                if(abs(n->balance) >= 2){
                    if(print == 1)
                        printTree(t->root,0);
                    n = treeRotation(print,t, n, father);
                }
                if (n->left_depth >= n->right_depth){
                    new_depth = n->left_depth;
                } else {
                    new_depth = n->right_depth;
                }
                return (new_depth + 1);
            }
        } else {
            if ((n->left_son == NULL) && (n->right_son == NULL)){
                if (print == 1){
                    printf("Uma folha foi atingida e o no nao foi encontrado na arvore.\n\n");
                }
                return -1;
            } else {
                if (new->value > n->value){
                    n->right_depth = findNodePosition(mode,print,t,n->right_son,new,depth,n);
                } else {
                    n->left_depth = findNodePosition(mode,print,t,n->left_son,new,depth,n);
                }
                n->balance = n->left_depth - n->right_depth;
                if(abs(n->balance) >= 2){
                    if(print == 1)
                        printTree(t->root,0);
                    n = treeRotation(print,t, n, father);
                }
                if (n->left_depth >= n->right_depth){
                    new_depth = n->left_depth;
                } else {
                    new_depth = n->right_depth;
                }
                return (new_depth + 1);
            }
        }
    }
}
