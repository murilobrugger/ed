#include <stdlib.h>
#include <stdio.h>

struct lista{
	int nivel;
	int valor;
	struct lista * esq;
	struct lista * dir;
} typedef Lista;

void printaArvore(Lista * arvore){
	if(arvore != NULL){
		printf("Valor= %d ",arvore->valor);
		printf("Nivel= %d\n",arvore->nivel);
		printaArvore(arvore->esq);
		printaArvore(arvore->dir);
	}
}

void calculaNivel(int nivel, Lista * arvore){
	if(arvore != NULL){
		arvore->nivel = nivel;
		calculaNivel(nivel+1,arvore->esq);
		calculaNivel(nivel+1,arvore->dir);
	}
}

void main(){
	Lista * arvore = (Lista*)malloc(sizeof(Lista));
	Lista * esq = (Lista*)malloc(sizeof(Lista));
	Lista * dir = (Lista*)malloc(sizeof(Lista));

	arvore->valor = 1;
	dir->valor = 2;
	esq->valor = 3;
	arvore->esq = esq;
	arvore->dir = dir;
	esq->esq = NULL;
	esq->dir = NULL;
	dir->esq = NULL;
	dir->dir = NULL;
	calculaNivel(0, arvore);
	printaArvore(arvore);
}