#include <stdlib.h>
#include <stdio.h>
#include <math.h>

void printArray(int * array, int size){
	printf("[");
	for(int i = 0; i < size; i++){
		printf("%d ", array[i]);
	}
	printf("]\n");
}

int partition(int begin, int end, int * array){
	int diff = end - begin;
	int pivot =  (int)(rand() % diff) + begin;
	int left = begin;
	int right = end;
	int run = 0;
	while(run == 0){
		for(left; left <= pivot; left++){
			printf("Left: %d\n",left);
			if(array[left] >= array[pivot]){
				for(right; right >= pivot; right--){
					printf("Right: %d\n",right);
					if(array[right] <= array[pivot]){
						if(right == left){
							run = 1;
						}
						int aux = array[right];
						array[right] = array[left];
						array[left] = aux;
						if(right == pivot){
							pivot = left;
						} else if(left == pivot){
							pivot = right;
						}
						left = begin;
						right = end;
						break;
					}
				}
				break;
			}
		}

	}
	
	return pivot;
}

void quicksort(int begin, int end, int * array){
	if(end - begin >= 1){
		int pivot = partition(begin, end, array);
		quicksort(begin, pivot - 1, array);
		if(end - pivot > 1)
			quicksort(pivot, end, array);
	}
}

void generateArray(int * array,int size, int maxRange){
	for(int i = 0; i < size; i++){
		array[i] = (int)(rand() % (maxRange + 1));
	}
}

void main(){
	int array[100] = {53, 14, 76, 47, 54, 31, 41, 40, 89, 23, 61, 96, 51, 59, 45,  5, 55, 27, 28, 98, 33, 42,  2,  7, 20,  8, 62, 99, 70, 78, 24, 58, 85,  6, 93, 94, 35, 12, 15, 13,  1, 81, 95, 72, 38, 87, 68, 86, 80, 43, 29, 97, 88, 37, 57, 21, 84, 17, 71, 92, 22, 79, 30, 91, 52, 63, 50, 16, 65, 10, 44, 83, 34, 39, 32, 67, 75, 11,  9,  4, 77, 19, 82, 26,100, 90, 18,  3, 36, 46, 66, 60, 49, 69, 64, 48, 25, 73, 56, 74};
	quicksort(0, 99, array);
	printArray(array, 100);

}


