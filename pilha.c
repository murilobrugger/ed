#include <stdio.h>
#include <stdlib.h>


struct item{
	int id;
	struct item* lower;
} typedef Item;

struct pilha {
	Item *top;
	int size;
	int maxSize;
} typedef Pilha;

Pilha criaPilha(int maxSize){
	Pilha novapilha = {NULL, 0, maxSize};
	return novapilha;
}

Item* pop(Pilha* pilha){
	if(pilha->size == 0){
		return NULL;
	}
	pilha->size--;
	Item* resposta = pilha->top;
	pilha->top = resposta->lower;
	return resposta;
}

int push(Item* item, Pilha * pilha){
	if (pilha->size <= pilha->maxSize) {
		item->lower = pilha->top;
		pilha->top = item;
		pilha->size = pilha->size + 1;
		return 0;
	}
	return 1;
}


int main() {
	Pilha pilhaTeste = criaPilha(10);
	Item item1 = {1,NULL};
	Item item2 = {2,NULL};

	push(&item1, &pilhaTeste);
	push(&item2, &pilhaTeste);
	Item resp1 = *pop(&pilhaTeste);
	Item resp2 = *pop(&pilhaTeste);

	printf("Resposta 1 = %d \n", resp1.id);
	printf("Resposta 2 = %d \n", resp2.id);
}


