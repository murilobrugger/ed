#include <stdlib.h>
#include <stdio.h>

void printArray(int * array, int size){
	printf("[");
	for(int i = 0; i < size; i++){
		printf("%d ", array[i]);
	}
	printf("]\n");
}

void merge(int e, int m, int d, int * array){
	int * aux = (int*)malloc(sizeof(int) * (d - e));
	for(int i = 0; i <= (d - e); i++){
		aux[i] = array[e + i];
	}
	int e_i = m - e;
	int d_i = d - e;

	for(int i = d; i >= e; i--){
		if(e_i < 0){
			array[i] = aux[d_i];
			d_i--;
		} else if(d_i <= m - e){
			array[i] = aux[e_i];
			e_i--;	
		} else if(aux[d_i] < aux[e_i]){
			array[i] = aux[e_i];
			e_i--;
		} else{
			array[i] = aux[d_i];
			d_i--;
		}
	}
	free(aux);
	// getchar();
}

void MergeSort(int e, int d, int * array){
	if(e < d){
		
		int middle = (e + d)/2;
		MergeSort(e, middle, array);
		MergeSort(middle + 1, d, array);
		merge(e, middle, d, array);
	}
}

void main(){
	int array[20] = {4,3,2,1,5,12,11,6,8,22,33,24,0,45,65,62,46,41,99,13};
	MergeSort(0, sizeof(array) / sizeof(int) - 1, array);
	printArray(array, sizeof(array) / sizeof(int));
}