#include <stdlib.h>
#include <stdio.h>

#define INFINITY 999999999999999999999999

int findSize(char* array){
	for (int i = 0; i < INFINITY; i++){
		if(array[i] == '\0'){
			return i;
		}
	}
}

void main(){

	char ** nomes = (char**)malloc(sizeof(sizeof(char*) * 6) * 10);
	for(int i = 0; i < 10; i++){
		char nome[6] = {'a', 'b', 'c', 'd', 'e', '\0'};
		nomes[i] = nome;
	}

	for(int i = 0; i < 10; i++){
		// for(int j = 0; j < 5 ; j++){
			int size = findSize(nomes[i]);
			printf("%d, ",size);
		// }
		printf("\n");
	}
}