#include <stdlib.h>
#include <stdio.h>

struct lista{
	int nivel;
	int valor;
	struct lista * esq;
	struct lista * dir;
} typedef Lista;

void printaArvore(Lista * arvore){
	if(arvore != NULL){
		printf("Valor= %d ",arvore->valor);
		printf("Nivel= %d\n",arvore->nivel);
		printaArvore(arvore->esq);
		printaArvore(arvore->dir);
	}
}

void pegaNivel(Lista* pai, Lista ** filhos){
	filhos[0] = pai->esq;
	filhos[1] = pai->dir;
}  

void percorreNivel(Lista ** nivel, int size){
	int newSize = size * 2;
	
}

void main(){
	Lista * arvore = (Lista*)malloc(sizeof(Lista));
	Lista * esq = (Lista*)malloc(sizeof(Lista));
	Lista * dir = (Lista*)malloc(sizeof(Lista));
	Lista * esq1 = (Lista*)malloc(sizeof(Lista));
	Lista * dir1 = (Lista*)malloc(sizeof(Lista));

	arvore->valor = 1;
	dir->valor = 2;
	esq->valor = 3;
	dir1->valor = 4;
	esq1->valor = 5;
	arvore->esq = esq;
	arvore->dir = dir;
	esq->esq = esq1;
	esq->dir = dir1;
	dir->esq = NULL;
	dir->dir = NULL;
	esq1->esq = NULL;
	esq1->dir = NULL;
	dir1->esq = NULL;
	dir1->dir = NULL;
	Lista ** filhos = (Lista**)malloc(sizeof(Lista*) * 2);
	pegaNivel(arvore,filhos);
	percorreNivel(filhos, 2);
	// printaArvore(arvore);
}