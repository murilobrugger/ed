#include <stdlib.h>
#include <stdio.h>

struct elemento {
	int value;
	struct elemento * next;
} typedef Elemento;

struct fila {
	int maxSize;
	int size;
	Elemento * start;
	Elemento * end;
} typedef Fila;

Fila criaFila(int maxSize){
	Fila novaFila = {maxSize, 0, NULL, NULL};
	return novaFila;
}

int insereFila(Elemento * elemento, Fila* fila){
	if( fila->size == 0){
		fila->start = elemento;
		fila->end = elemento;
		fila->size++;
		return 1;
	} else if( fila->size + 1 <= fila->maxSize){
		fila->end->next = elemento;
		fila->end = elemento;
		fila->size++;
		return 1;
	}
	return 0;
}

Elemento * removeFila(Fila* fila) {
	if (fila->end == fila->start){
		Elemento * retorno = fila->start;
		fila->start = NULL;
		fila->end = NULL;
		fila->size--;
		return retorno;
	} else if(fila->size > 0){
		Elemento * retorno = fila->start;
		fila->start = retorno->next;
		fila->size--;
		return retorno;
	} else{
		return NULL;
	}
}

int main(){
	Fila filaTeste = criaFila(10);

	Elemento elem1 = { 35, NULL };
	Elemento elem2 = { 40, NULL };
	
	insereFila(&elem1, &filaTeste);
	insereFila(&elem2, &filaTeste);

	Elemento resp1 = *removeFila(&filaTeste);
	Elemento resp2 = *removeFila(&filaTeste);

	printf("Resp1 %d \n", resp1.value);
	printf("Resp2 %d \n", resp2.value);
	return 0;
}










